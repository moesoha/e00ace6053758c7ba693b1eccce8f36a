#!/bin/sh

if [ $# -ne 1 ]
	then
		echo take port number as argument
		exit 1
	fi

apt update && apt install -y git curl build-essential libssl-dev zlib1g-dev

git clone https://github.com/TelegramMessenger/MTProxy
cd MTProxy
make && cp -vr objs/bin /opt/MTProxy

curl -s https://core.telegram.org/getProxySecret -o /opt/MTProxy/proxy-secret
curl -s https://core.telegram.org/getProxyConfig -o /opt/MTProxy/proxy-multi.conf

SECRET=$(head -c 16 /dev/urandom | xxd -ps)

cat << EOF > /etc/systemd/system/MTProxy.service
[Unit]
Description=MTProxy
After=network.target

[Service]
Type=simple
WorkingDirectory=/opt/MTProxy
ExecStart=/opt/MTProxy/mtproto-proxy -u nobody -p 9993 -H $1 -S $SECRET --aes-pwd /opt/MTProxy/proxy-secret /opt/MTProxy/proxy-multi.conf
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable MTProxy.service
systemctl restart MTProxy.service
# systemctl status MTProxy.service

echo -e "12 0 * * *\troot\tcurl -s https://core.telegram.org/getProxyConfig -o /opt/MTProxy/proxy-multi.conf && systemctl restart MTProxy" >> /etc/crontab
systemctl restart cron.service

echo "========================"
echo "Access the MTProto proxy"
echo "with these argument:"
echo "Port: $1"
echo "Secret:"
echo $SECRET
echo "Secret(dynamic padding):"
echo "dd$SECRET"
echo "========================"
echo "Don't forget to register"
echo "your proxy @MTProxybot"
